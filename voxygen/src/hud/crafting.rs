use super::{
    get_quality_col,
    img_ids::{Imgs, ImgsRot},
    item_imgs::{animate_by_pulse, ItemImgs, ItemKey::Tool},
    Show, TEXT_COLOR, TEXT_DULL_RED_COLOR, TEXT_GRAY_COLOR, UI_HIGHLIGHT_0, UI_MAIN,
};
use crate::ui::{
    fonts::Fonts, ImageFrame, ItemTooltip, ItemTooltipManager, ItemTooltipable, Tooltip,
    TooltipManager, Tooltipable,
};
use client::{self, Client};
use common::{
    assets::AssetExt,
    comp::{
        item::{
            ItemDef, ItemDesc, ItemKind, ItemTag, MaterialStatManifest, Quality, TagExampleInfo,
        },
        Inventory,
    },
    recipe::{Recipe, RecipeInput},
    terrain::SpriteKind,
};
use conrod_core::{
    color, image,
    position::Dimension,
    widget::{self, Button, Image, Rectangle, Scrollbar, Text, TextEdit},
    widget_ids, Color, Colorable, Labelable, Positionable, Sizeable, Widget, WidgetCommon,
};
use i18n::Localization;
use std::sync::Arc;

use strum::IntoEnumIterator;
use strum_macros::EnumIter;

widget_ids! {
    pub struct Ids {
        window,
        window_frame,
        close,
        icon,
        title_main,
        title_rec,
        align_rec,
        scrollbar_rec,
        btn_open_search,
        btn_close_search,
        input_search,
        input_bg_search,
        input_overlay_search,
        title_ing,
        tags_ing[],
        category_bgs[],
        category_tabs[],
        category_imgs[],
    }
}

impl PetTab {
    fn name_key(self) -> &'static str {
        match self {
            PetTab::one => "hud.pet.tabs.one",
            PetTab::two => "hud.pet.tabs.two",
            PetTab::thr => "hud.pet.tabs.thr",
            PetTab::fou => "hud.pet.tabs.fou",
            PetTab::fiv => "hud.pet.tabs.fiv",
            PetTab::six => "hud.pet.tabs.six",
            PetTab::sev => "hud.pet.tabs.sev",
            PetTab::eig => "hud.pet.tabs.eig",
            PetTab::nin => "hud.pet.tabs.nin",
            PetTab::ten => "hud.pet.tabs.ten",
        }
    }

    fn img_id(self, imgs: &Imgs) -> image::Id {
        match self {
            PetTab::one => //PET PIC,
            PetTab::two => //PET PIC,
            PetTab::thr => //PET PIC,
            PetTab::fou => //PET PIC,
            PetTab::fiv => //PET PIC,
            PetTab::six => //PET PIC,
            PetTab::sev => //PET PIC,
            PetTab::eig => //PET PIC,
            PetTab::nin => //PET PIC,
            PetTab::ten => //PET PIC,
        }
    }


impl<'a> Widget for Pet<'a> {
    type Event = Vec<Event>;
    type State = State;
    type Style = ();

    fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
        State {
            ids: Ids::new(id_gen),
            selected_pet: None,
        }
    }
}

    fn style(&self) -> Self::Style {}
